self.port.on("decorateOffers", function (blacklist) {
  console.log("decorateOffers are called");
  var offers = getOffersList();
  if (offers.length) {
    console.log("decorateOffers: offers are loaded");
    abc();

  }
  for(var i=0; i<offers.length; ++i) {
    var offer = new Offer(offers[i]);
    var offerShouldHide = blacklist.find(function(hiddenOffer) {
      return hiddenOffer === offer.getID()
    });

    if(offerShouldHide) {
      hiddenOffers[offerShouldHide] = offer;
      offer.hide();
    }

    offer.decorate();
  }
});

 // listen click search button
  // var button = document.getElementById("submit-filters");
  // if (button) {
  //   button.addEventListener("click", function(event) {
  //     self.port.emit("reloadOffers");
  //   });
  // }

self.port.on("showOffer", function(offerId) {
  var offer = hiddenOffers[offerId];
  if(offer) {
    offer.show();
  }
});

var hiddenOffers = {};

function getOffersList() {
  var offersClassName = "offers list";
  var offerClassName = "offer-item is-row";

  var offers = document.getElementsByClassName(offersClassName);
  if (offers !== null && offers.length > 0) {
    return offers[0].getElementsByClassName(offerClassName);
  } else {
    return [];
  }
}

function hideOffer(event) {
  var offer = new Offer(event.target.parentElement.parentElement.parentElement.parentElement);
  hiddenOffers[offer.getID()] = offer;
  offer.hide();
  self.port.emit("addToBlacklist", offer.getID());
}

function Offer(offer) {
  this.offer = offer;

  this.getID = function() {
    var offerTitleLink = this.offer.getElementsByClassName("offer-title__link");
    if(!offerTitleLink && offerTitleLink.length > 0) {
      return null;
    }

    var id = offerTitleLink[0].getAttribute("data-ad-id")
    return id;
  }

  this.show = function() {
    this.offer.style.visibility = "visible";
    this.offer.style.display = 'block';
  }

  this.hide = function() {
    this.offer.style.visibility = "hidden";
    this.offer.style.display = 'none';
  }

  this.decorate = function() {
    var offerTitle = this.offer.getElementsByClassName("offer-title")[0];
    var offerId = this.getID();

    var hideDecorator = document.createElement("a");
    hideDecorator.innerHTML="[Hide]";
    hideDecorator.addEventListener("click", hideOffer);
    offerTitle.appendChild(hideDecorator);
  }
}

function areAnnouncementsRefreshed() {
  return getOffersList().length > 0;
}

function abc() {
  
  var s_ajaxListener = new Object();
s_ajaxListener.tempOpen = XMLHttpRequest.prototype.open;
s_ajaxListener.tempSend = XMLHttpRequest.prototype.send;
s_ajaxListener.callback = function () {
  // this.method :the ajax method used
  // this.url    :the url of the requested script (including query string, if any) (urlencoded) 
  // this.data   :the data sent, if any ex: foo=bar&a=b (urlencoded)
  //alert(JSON.stringify(this));
  //alert('abc');
  self.port.emit("reloadOffers");
  
  
}

XMLHttpRequest.prototype.open = function(a,b) {
  if (!a) var a='';
  if (!b) var b='';
  s_ajaxListener.tempOpen.apply(this, arguments);
  s_ajaxListener.method = a;  
  s_ajaxListener.url = b;
  if (a.toLowerCase() == 'get') {
    s_ajaxListener.data = b.split('?');
    s_ajaxListener.data = s_ajaxListener.data[1];
  }
  
  this.onreadystatechange = function() {
    
    if (this.readyState == 4 && this.status == 200) {
       // Typical action to be performed when the document is ready:
       //alert(document.getElementsByClassName('listOverlay').length);
       self.port.emit("reloadOffers");
    }
  };
}

XMLHttpRequest.prototype.send = function(a,b) {
  if (!a) var a='';
  if (!b) var b='';
  s_ajaxListener.tempSend.apply(this, arguments);
  if(s_ajaxListener.method.toLowerCase() == 'post')s_ajaxListener.data = a;
  s_ajaxListener.callback();
}

console.log('register ajax event');
}