self.port.on("showBlacklist", function(offers) {
  console.log("showBlacklistCalled");

  var node = document.getElementById("blacklist");
  while (node.hasChildNodes()) {
      node.removeChild(node.lastChild);
  }

  for(var i=0; i<offers.length; i++) {
    addOffer(offers[i]);
  }
});

function removeFromBlacklist(event) {
  self.port.emit("removeFromBlacklist", event.target.innerHTML);
  var targetOffer = event.target;
  targetOffer.parentNode.removeChild(targetOffer);
}

function addOffer(offerId) {
  var offer = document.createElement("li");
      offer.innerHTML=offerId;
      offer.addEventListener("click", removeFromBlacklist);
      document.getElementById("blacklist").appendChild(offer);
}