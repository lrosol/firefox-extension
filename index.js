var buttons = require('sdk/ui/button/action');
var tabs = require("sdk/tabs");
var panels = require("sdk/panel");
var self = require("sdk/self");
var simpleStorage = require('sdk/simple-storage');

var blacklist = new Blacklist(simpleStorage);

var { ToggleButton } = require('sdk/ui/button/toggle');

var toolbarButton = ToggleButton({
  id: "toolbar-button",
  label: "Otomoto blacklist",
  icon: {
    "16": "./icon-16.png",
    "32": "./icon-32.png",
    "64": "./icon-64.png"
  },
  onChange: function(state) {
    if(state.checked) {
      blacklistView.show({
        position: toolbarButton
      });

      blacklistView.port.emit("showBlacklist", blacklist.getOffers());
    }
  }
});

var pageMod = require("sdk/page-mod");
var activeTabWorker;

// decorate announcements
activeTabWorker = pageMod.PageMod({
  include: "*.otomoto.pl",
  contentScriptFile: self.data.url("decorator.js"),
  onAttach: function(worker) {
    worker.port.emit("decorateOffers", blacklist.getOffers());
    worker.port.on("addToBlacklist", function(id) {
      blacklist.add(id);
    });
    worker.port.on("reloadOffers", function() {
      console.log("reload called");
      worker.port.emit("decorateOffers", blacklist.getOffers());
    });
  }
});

// UI control connected to toggle button
var blacklistView = panels.Panel({
	contentURL: self.data.url("panel.html"),
  contentScriptFile: self.data.url("panel.js"),
	onHide: function() {
    toolbarButton.state('window', {checked: false});
  }
});

// listener for panel
blacklistView.port.on("removeFromBlacklist", function(offerId) {
  console.log("removeFromBlacklist called: " + offerId);
  activeTabWorker.port.emit("showOffer", offerId);
  blacklist.remove(offerId);
});





function Blacklist(storage) {
  // initialize empty storage
  if (!storage.storage.offers) {
    storage.storage.offers = [];
  }

  var offers = simpleStorage.storage.offers;
  var sizeBeforeOperation, sizeAfterOperation;

  this.add = function(element) {
    sizeBeforeOperation = offers.length;
    offers.push(element);
    sizeAfterOperation = offers.length;
    console.log("Blacklist: Added: " + element + " Current size: " + offers.length);
    return sizeBeforeOperation === sizeAfterOperation + 1;
  }

  this.remove = function(element) {
    sizeBeforeOperation = offers.length;
    offers.pop(element);
    sizeAfterOperation = offers.length;
    console.log("Blacklist: Removed: " + element + "Current size: " + offers.length);
    return sizeBeforeOperation === sizeAfterOperation - 1;
  }

  this.length = function() {
    return offers.length;
  }

  this.getOffers = function() {
    return offers;
  }
}